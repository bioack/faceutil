#!/usr/bin/env python3

# python3 roc2img.py actives.txt scores.txt roc.png

import sys
import csv
import os
from operator import itemgetter
import matplotlib.pyplot as plt
import numpy as np

from sklearn.metrics import auc


def DepictROCCurve(actives, scores, label, color, fname, randomline=True):
    plt.figure(figsize=(4, 4), dpi=80)
    SetupROCCurvePlot(plt)
    AddROCCurve(plt, actives, scores, color, label)
    # add another
    actives = actives[2:]
    scores = scores[2:]
    color = "#008088"  # dark green

    AddROCCurve(plt, actives, scores, color, label)
    SaveROCCurvePlot(plt, fname, randomline)


def read_tp_fp(fname):
    tpr = []
    fpr = []
    with open(fname, newline='') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        next(csv_reader, None)  # skip the headers
        for row in csv_reader:
            tp = float(row[1])
            tn = float(row[2])
            fp = float(row[3])
            fn = float(row[4])
            tpr.append(tp / (tp + fn))
            fpr.append(fp / (tn + fp))

    tpr.append(1.0)
    fpr.append(1.0)
    tpr = sorted(tpr)
    fpr = sorted(fpr)
    return tpr, fpr


def addROCCurve2(plt, tpr, fpr, linestyle, color, label):
    plt.plot(fpr, tpr, linestyle=linestyle, color=color, linewidth=2, label=label)


color_list = [
    'blue',
    'green',
    'darkolivegreen',
    'cyan',
    'magenta',
    'yellow',
    'black',
    'aqua'
]

FLOAT_FORMAT = "{0:.4f}"


def main2(mpath, fname):
    plt.figure(figsize=(4, 4), dpi=80)
    SetupROCCurvePlot(plt)
    files = os.listdir(mpath)
    ix = 0
    results = []
    for f in files:
        name = f
        path = mpath + "/" + name
        tpr, fpr = read_tp_fp(path)
        results.append([auc(fpr, tpr), name[:name.index('.csv')], tpr, fpr])

    results = sorted(results, key=lambda arr: arr[0])

    for item in results:
        aucv = item[0]
        name = item[1]
        tpr = item[2]
        fpr = item[3]
        label = name + '(auc=' + FLOAT_FORMAT.format(aucv) + ')'
        if 'zihui' in name:
            linestyle = '--'
        else:
            linestyle = '-'
        addROCCurve2(plt, tpr, fpr, linestyle=linestyle, color=color_list[ix], label=label)
        ix += 1
    SaveROCCurvePlot(plt, fname, randomline=True)


def main(argv=[__name__]):
    if len(sys.argv) != 4:
        print("Usage: <actives> <scores> <image>")
        return 1

    afname = sys.argv[1]
    sfname = sys.argv[2]
    ofname = sys.argv[3]

    f, ext = os.path.splitext(ofname)
    if not IsSupportedImageType(ext):
        print("Format \"%s\" is not supported!" % ext)
        return 1

    # read id of actives

    actives = LoadActives(afname)
    print("Loaded %d actives from %s" % (len(actives), afname))

    # read molecule id - score pairs

    label, scores = LoadScores(sfname)
    print("Loaded %d %s scores from %s" % (len(scores), label, sfname))

    # sort scores by ascending order
    sortedscores = sorted(scores, key=itemgetter(1))

    print("Plotting ROC Curve ...")
    color = "#008000"  # dark green
    DepictROCCurve(actives, sortedscores, label, color, ofname)

    return 0


def LoadActives(fname):
    actives = []
    for line in open(fname, 'r').readlines():
        id = line.strip()
        actives.append(id)

    return actives


def LoadScores(fname):
    sfile = open(fname, 'r')
    label = sfile.readline()
    label = label.strip()

    scores = []
    for line in sfile.readlines():
        id, score = line.strip().split()
        scores.append((id, float(score)))

    return label, scores


def GetRates(actives, scores):
    tpr = [0.0]  # true positive rate
    fpr = [0.0]  # false positive rate
    nractives = len(actives)
    nrdecoys = len(scores) - len(actives)

    foundactives = 0.0
    founddecoys = 0.0
    for idx, (id, score) in enumerate(scores):
        if id in actives:
            foundactives += 1.0
        else:
            founddecoys += 1.0

        tpr.append(foundactives / float(nractives))
        fpr.append(founddecoys / float(nrdecoys))

    return tpr, fpr


def SetupROCCurvePlot(plt):
    plt.xlabel("FPR", fontsize=14)
    plt.ylabel("TPR", fontsize=14)
    plt.title("ROC Curve", fontsize=14)


def SaveROCCurvePlot(plt, fname, randomline=True):
    if randomline:
        x = [0.0, 1.0]
        plt.plot(x, x, linestyle='dashed', color='red', linewidth=2, label='random')

    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)
    # plt.xticks(np.arange(0, 1.1, 0.1))
    # plt.legend(fontsize=9, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.legend(fontsize=9, loc='best')
    plt.tight_layout()
    plt.savefig(fname)


def AddROCCurve(plt, actives, scores, color, label):
    tpr, fpr = GetRates(actives, scores)

    plt.plot(fpr, tpr, color=color, linewidth=2, label=label)


def DepictROCCurve(actives, scores, label, color, fname, randomline=True):
    plt.figure(figsize=(6000, 1000), dpi=96)
    SetupROCCurvePlot(plt)
    AddROCCurve(plt, actives, scores, color, label)
    # add another
    actives = actives[2:]
    scores = scores[2:]
    color = "#008088"  # dark green

    AddROCCurve(plt, actives, scores, color, label)
    SaveROCCurvePlot(plt, fname, randomline)


def IsSupportedImageType(ext):
    fig = plt.figure()
    return (ext[1:] in fig.canvas.get_supported_filetypes())


if __name__ == "__main__":
    # sys.exit(main(sys.argv))
    # sys.exit(main2(sys.argv))
    # main2('me', 'roc.png')
    main2('lightenedcnn_performance', 'lightenedcnn_performance2.png')
