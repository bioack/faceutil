import csv

# path = "./feature_vector_facenet_lfw.csv"
# vector_path = "./facenet_lfw_vector.csv"
# label_path = './facenet_lfw_label.csv'

path = "./feature_vector_mxnet_lfw.csv"
vector_path = "./mxnet_lfw_vector.csv"
label_path = './mxnet_lfw_label.csv'

def preprocess(fname):
    with open(fname, newline='') as csv_file, \
            open(vector_path, 'a') as vector_file, \
            open(label_path, 'a') as label_file:
        csv_reader = csv.reader(csv_file, delimiter=";")
        for row in csv_reader:
            vector_file.write(row[0] + ';' + row[2].replace(',', ";") + '\n')
            label_file.write(row[0] + ";" + row[1] + '\n')


preprocess(path)

# with open(path) as file:
#     with open(out_path, 'a') as out_file:
#         for l in file:
#             l = l.replace(",", ";")
#             out_file.write(l)
