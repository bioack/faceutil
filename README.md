


## Deps

- python 3
```shell 
sudo pip3 install mysql-connector==2.1.4
```


## TODO

- `alignment`
- `eqHist`
- verify `float` vs `np.float64`
- cleanup & organize this as a module + util
- use virtualenv
