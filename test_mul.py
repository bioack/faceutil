from multiprocessing import Pool
from multiprocessing import Process, Queue
import os


def f(x):
    info(str(x))
    return x * x


class A:
    def __init__(self, a):
        self.a = a


def fn(name, q):
    q.put(A(name))
    print('hello ', name)


def info(title):
    print(title)
    print('module name: ', __name__)
    print('parent process: ', os.getppid())
    print('process id:', os.getpid())


if __name__ == '__main__':
    # with Pool(5) as p1:
    #     print(p1.map(f, [1, 2, 3]))

    q = Queue()
    p1 = Process(target=fn, args=('bob', q))
    p1.start()
    p2 = Process(target=fn, args=('wow', q))
    p2.start()
    print(q.get().a)
    print(q.get().a)
    p1.join()
    p2.join()
