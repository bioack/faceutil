import csv
import logging
import time

import mysql.connector as mariadb
import numpy as np
import itertools
from mysql.connector import errorcode
from scipy.spatial.distance import cosine
from sklearn.metrics import auc

from multiprocessing import Process, Queue

L = logging.getLogger("faceutil")
L.setLevel(logging.INFO)

PARALLEL_NUM = 6

DB_NAME = 'face'

mysql_config = {
    'user': 'admin',
    'password': 'passw0rd',
    'host': '127.0.0.1',
    # 'database': 'face',
    'raise_on_warnings': True,
}


# cnx = mariadb.connect(**mysql_config)
# cnx.autocommit = True
# cursor = cnx.cursor()


def mariadb_create_database(cursor, dname):
    try:
        cursor.execute("CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(dname))
    except mariadb.Error as err:
        L.error("failed creating database: {}".format(err))
        exit(1)


def mariadb_create_face_table():
    tables = {
        'facenet_unaligned_unfilterted': (
            "CREATE TABLE `facenet_unaligned_unfilterted` ("
            "  `id` int(11) NOT NULL AUTO_INCREMENT,"
            "  `ix` int NOT NULL,"
            "  `label` int NOT NULL,"
            "  `name` varchar(50) NOT NULL,"
            "  `vector` varchar(6000) NOT NULL,"
            "  PRIMARY KEY (`id`)"
            ") ENGINE=InnoDB")
    }
    for name, ddl in tables.items():
        try:
            L.info("Creating table {}: ".format(name))
            cursor.execute(ddl)
        except mariadb.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                L.info("\talready exists.")
            else:
                L.error(err.msg)
        else:
            L.info("\tOK")


add_face_ddl = ("INSERT INTO face "
                "(ix, label, name, vector) "
                "VALUES (%s, %s, %s, %s)")


def add_face(face):
    cursor.execute(face)
    no = cursor.lastrowid
    print(no)


def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step


# todo make it general
def gen_threshold(start=0.0, end=1.0, step=0.01):
    start_int = int(start * 100)
    end_int = int(end * 100)
    step_int = int(step * 100)
    return (x / 100 for x in range(start_int, end_int, step_int))


class Subject:
    def __init__(self, label, name, M):
        self.label = label
        self.name = name
        self.M = M
        self.len = len(M)

    def add_m(self, m):
        self.M = np.append(self.M, m, axis=0)
        self.len += 1


class EvaluationMetrics:
    def __init__(self, threshold_set=gen_threshold()):
        self.metrics_set = [Metrics(th) for th in threshold_set]

    def roc(self):
        pass

    def auc(self):
        pass

    def draw(self):
        pass

    def __str__(self):
        return '\n'.join(str(m) for m in self.metrics_set)

    def merge_from(self, other):
        for m in self.metrics_set:
            for o in other.metrics_set:
                m.merge_from(o)

    def dump(self, fname):
        with open(fname, 'w') as f:
            if len(self.metrics_set) > 0:
                # todo convert it to static method
                f.write(str(self.metrics_set[0].dumph) + '\n')
            f.write('\n'.join(m.dump() for m in self.metrics_set))


class Metrics:
    def __init__(self, threshold):
        self.threshold = threshold
        self.tp = 0
        self.tn = 0
        self.fp = 0
        self.fn = 0

    def merge_from(self, other):
        if other.threshold == self.threshold:
            self.tp += other.tp
            self.tn += other.tn
            self.fp += other.fp
            self.fn += other.fn
        else:
            pass

    def tpp1(self):
        self.tp += 1

    def tnp1(self):
        self.tn += 1

    def fpp1(self):
        self.fp += 1

    def fnp1(self):
        self.fn += 1

    @staticmethod
    def safe_divide(a, b):
        if b == 0.0:
            return float('nan')
        else:
            return a / b

    @property
    def rate_accuracy(self):
        return self.safe_divide(self.tp + self.tn, self.tp + self.tn + self.fp + self.fn)

    def rate_error(self):
        return 1 - self.rate_accuracy

    @property
    def rate_precision(self):
        return self.safe_divide(self.tp, self.tp + self.fp)

    @property
    def rate_sensitivity(self):
        return self.rate_tpr

    @property
    def rate_recall(self):
        return self.rate_tpr

    @property
    def rate_tpr(self):
        return self.safe_divide(self.tp, self.tp + self.fn)

    @property
    def rate_fpr(self):
        return self.safe_divide(self.fp, self.fp + self.tn)

    @property
    def rate_tnr(self):
        return self.safe_divide(self.tn, self.tn + self.fp)

    @property
    def rate_fnr(self):
        return self.safe_divide(self.fn, self.fn + self.tp)

    # miss rate = 1 - rate_tpr
    @property
    def rate_miss(self):
        return self.rate_fnr

    @property
    def dumph(self):
        return "threshold,tp,tn,fp,fn,accuracy,precision,recall(tpr),fpr"

    def dump(self, sep=','):
        return '{},{},{},{},{},{:.6},{:.6},{:.6},{:.6}' \
            .format(self.threshold,
                    self.tp,
                    self.tn,
                    self.fp,
                    self.fn,
                    self.rate_accuracy,
                    self.rate_precision,
                    self.rate_recall,
                    self.rate_fpr)

    def __str__(self):
        return "threshold={},tp={},tn={},fp={},fn={},accuracy={:.2%},precision={:.2%},recall={:.2%}" \
            .format(self.threshold,
                    self.tp,
                    self.tn,
                    self.fp,
                    self.fn,
                    self.rate_accuracy,
                    self.rate_precision,
                    self.rate_recall)


def np_from_str(fstr):
    return np.fromstring(fstr, dtype=float, sep=',')


def csv_to_subject(fname, take_face_first=None):
    subject_dict = {}
    subject_mapping = {}
    with open(fname, newline='') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=";")
        i = 0
        ix = 0
        for row in csv_reader:
            i += 1
            ix += 1
            label = int(row[0])
            if label in subject_dict:
                subject_mapping[ix] = (label, subject_dict[label].len)
                subject_dict[label].add_m(np.array([np_from_str(row[2])]))
            else:
                subject_mapping[ix] = (label, 0)
                subject_dict[label] = Subject(label, row[1], np.array([np_from_str(row[2])]))
            if take_face_first == i:
                return subject_dict, subject_mapping
    return subject_dict, subject_mapping


def cal_cosine_similarity(vector_a, vector_b):
    return 1 - cosine(vector_a, vector_b)


def read_data(fname):
    with open(fname, newline='') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=";")
        for row in csv_reader:
            print(row)


def np_load_csv(fname, vector_length):
    # type_str = 'int' + (',float' * int(vector_length))
    # M = np.loadtxt(fname, delimiter=';', dtype=type_str, unpack=False, ndmin=2)
    # return M
    return np.loadtxt(fname, delimiter=';')


def np_from_str(fstr):
    return np.fromstring(fstr, dtype=float, sep=',')


# simulator add face to lib
def split_train_test(label, M, top=1):
    unique_label, index, count = np.unique(label, return_counts=True, return_index=True)
    # index_sets = [np.argwhere(i == a) for i in np.unique(L)]
    total = len(label)
    all = range(0, total)
    test_ix = [i for i in all if not index.__contains__(i)]

    train_M = M[index[: None]]
    test_M = M[test_ix[: None]]
    return train_M, label[index], test_M, label[test_ix]


def recognize_max_cosine_sim(x, M):
    sim = 0.0
    ix = 0
    for i in range(M.shape[0]):
        cur = cal_cosine_similarity(x, M[i, :])
        if cur > sim:
            sim = cur
            ix = i
    return ix, sim


def evaluate(algorithm, fname, vlen):
    csv_path = fname
    vector_len = vlen
    # labels, subjects, vectors = np_load_csv(csv_path)
    print('loading data from %s' % csv_path)
    M = np_load_csv(csv_path, vector_len)
    label, Mat = M[:, 0], M[:, 1:]

    # index_sets = [np.argwhere(i == a) for i in np.unique(label)]

    # label = label.reshape((-1, 1))
    # ix = [i for i in range(1, len(label) + 1)]
    # label = np.insert(label, 0, ix, axis=1)
    # label = label.astype(np.int32)
    train_M, train_label, test_M, test_label = split_train_test(label, Mat)

    tp = 0
    fp = 0
    for i in range(test_M.shape[0]):
        real_label = test_label[i]
        start = time.time()
        predicted_ix, sim = recognize_max_cosine_sim(test_M[i, :], train_M)
        predicted_label = train_label[predicted_ix]
        print("real label", real_label, "\t\tpredicted label=", predicted_label, ", \t\tsim=", sim, "\t\ttime=",
              time.time() - start)
        if predicted_label == real_label:
            tp += 1
        else:
            fp += 1

    print(algorithm, "=>\t tp=", tp, "\tfp=", fp)


def cross_validation(model_name, fname, vector_len):
    print('evaluating %s' % model_name)
    M = np_load_csv(fname, vector_len)
    label, Mat = M[:, 0], M[:, 1:]
    cross_validatio_internal(label, Mat, 0.75)


def cross_validatio_internal(label, Mat, threshold):
    tp = 0
    tn = 0
    fp = 0
    fn = 0
    for i in range(len(label)):
        sv = Mat[i, :]
        train_Mat = np.hstack((Mat[:i, :], Mat[i:, :]))
        predicted_ix, sim = recognize_max_cosine_sim(sv, train_Mat)
        print(predicted_ix, sim)


cnt = 0


def compare_n_internal(subject_dict, metrics):
    for k in subject_dict.keys():
        for start_ix in range(0, subject_dict[k].len - 1):
            a = subject_dict[k].M[start_ix, :]
            for i in range(start_ix + 1, subject_dict[k].len):
                b = subject_dict[k].M[i, :]
                c = cal_cosine_similarity(a, b)
                for m in metrics.metrics_set:
                    if c >= m.threshold:
                        m.tpp1()
                    else:
                        m.fnp1()


def compare_n_one(subject_dict_n, subject_dict_all, metrics):
    for k in subject_dict_n.keys():
        for i in range(0, subject_dict_all[k].len):
            for j in subject_dict_all.keys():
                if k != j:
                    a = subject_dict_n[k].M[i, :]
                    b = subject_dict_all[j].M[0, :]
                    c = cal_cosine_similarity(a, b)
                    for m in metrics.metrics_set:
                        if c > m.threshold:
                            m.fpp1()
                        else:
                            m.tnp1()


def compare_one_one(subject_dict_one, metrics):
    keys = subject_dict_one.keys()
    comb = [x for x in itertools.combinations(keys, 2)]
    L.warning(str(comb))
    for co in comb:
        a = subject_dict_one[co[0]].M[0, :]
        b = subject_dict_one[co[1]].M[0, :]
        c = cal_cosine_similarity(a, b)
        for m in metrics.metrics_set:
            if c >= m.threshold:
                m.fpp1()
            else:
                m.tnp1()


def evaluate_v2(subject_dict):
    metrics = EvaluationMetrics()
    subject_dict_n = {k: v for k, v in subject_dict.items() if v.len > 1}
    subject_dict_one = {k: v for k, v in subject_dict.items() if v.len == 1}
    compare_n_internal(subject_dict_n, metrics)
    compare_n_one(subject_dict_n, subject_dict, metrics)
    compare_one_one(subject_dict_one, metrics)
    return metrics


def cosine_similarity_dict(va, subject_dict, skip, metrics_set=None):
    global cnt
    max_sim = 0.0
    label = -1
    total = len(subject_dict)
    for i in range(skip[0], total + 1):
        for j in range(skip[1], subject_dict[i].len):
            if skip == (i, j):
                continue
            else:
                b = subject_dict[i].M[j, :]
                cnt += 1
                c = cal_cosine_similarity(va, b)

                for m in metrics_set.metrics_set:
                    if c >= m.threshold:
                        if i == skip[0]:
                            if subject_dict[i].label == label:
                                m.tpp1()
                            else:
                                m.fpp1()
                        else:
                            m.fpp1()
                    else:
                        if i == skip[0]:
                            m.fnp1()
                        else:
                            m.tnp1()
                if c > max_sim:
                    max_sim = c
                    label = subject_dict[i].label
    return label, max_sim


def cross_validation_dict(subject_dict, threshold_set=None):
    metrics = EvaluationMetrics()
    subject_len = len(subject_dict)
    for i in subject_dict.keys():
        L.warning("processing label %i", i)
        start = time.time()
        for j in range(0, subject_dict[i].len):
            if (i == subject_len) and (j == subject_dict[i].len - 1):
                break
            m = subject_dict[i].M[j, :]
            label, sim = cosine_similarity_dict(m, subject_dict, skip=(i, j), metrics_set=metrics)

            # for m in metrics.metrics_set:
            #     if sim >= m.threshold:
            #         if cnt > 1:
            #             if subject_dict[i].label == label:
            #                 m.tpp1()
            #             else:
            #                 m.fpp1()
            #         elif cnt == 1:
            #             m.fpp1()
            #         else:
            #             L.error("count invalid value %d", cnt)
            #     else:
            #         if cnt > 1:
            #             m.fnp1()
            #         elif cnt == 1:
            #             m.tnp1()
            #         else:
            #             L.error("count invalid value %d", cnt)
            L.warning("\t label=%f\tsim=%f time=%f", label, sim, (time.time() - start) * 1000)

    return metrics


def evaluate_v3(subject_dict, subject_mapping, combo, queue):
    metrics = EvaluationMetrics()
    total = len(combo)
    L.warning("total {} paris".format(len(combo)))
    cnt = 0
    for c in combo:
        cnt += 1
        if (cnt % 1000000) == 0:
            L.warning("\tprogress {}/{}".format(cnt, total))
        sa_ix = subject_mapping[c[0]]
        sb_ix = subject_mapping[c[1]]
        sa = subject_dict[sa_ix[0]]
        sb = subject_dict[sb_ix[0]]
        should_be_same = sa.label == sb.label
        a = sa.M[sa_ix[1], :]
        b = sb.M[sb_ix[1], :]
        sim = cal_cosine_similarity(a, b)
        for m in metrics.metrics_set:
            if sim >= m.threshold:
                if should_be_same:
                    m.tpp1()
                else:
                    m.fpp1()
            else:  # sim < threshold
                if should_be_same:
                    m.fnp1()
                else:
                    m.tnp1()
    queue.put(metrics)


# todo use fast version
def split_dict_equally(input_dict, chunks=3):
    # prep with empty dicts
    return_list = [dict() for idx in range(chunks)]
    idx = 0
    for k, v in input_dict.items():
        return_list[idx][k] = v
        if idx < chunks - 1:  # indexes start at 0
            idx += 1
        else:
            idx = 0
    return return_list


def split_list(l, chunks=3):
    step = int(len(l) / chunks)
    ll = []
    for i in range(chunks):
        if i == chunks - 1:
            ll.append(l[i * step:])
        else:
            ll.append(l[i * step:(i + 1) * step])

    assert len(l) == sum([len(x) for x in ll])
    return ll


def merge_metrics(metrics_list):
    mx = EvaluationMetrics();
    for m in metrics_list:
        mx.merge_from(m)
    return mx


def eval3(name, vname, target_dir):
    subject_dict, subject_mapping = csv_to_subject(vname)
    combo = [x for x in itertools.combinations(range(1, len(subject_mapping) + 1), 2)]
    combo_list = split_list(combo, chunks=PARALLEL_NUM)
    # subject_dict = csv_to_subject('ligntenedcnn_hist_eqv.csv')
    # metrics = cross_validation_dict(subject_dict)
    # splits = split_dict_equally(subject_mapping, chunks=PARALLEL_NUM)
    jobs = []
    queue = Queue()
    for i in range(PARALLEL_NUM):
        p = Process(target=evaluate_v3, args=(subject_dict, subject_mapping, combo_list[i], queue))
        p.start()
        jobs.append(p)

    for p in jobs:
        p.join()

    mx = []
    while not queue.empty():
        mx.append(queue.get())

    metrics = merge_metrics(mx)
    metrics.dump(target_dir + '/' + name + ".csv")


if __name__ == '__main__':
    # if len(sys.argv) < 2:
    #     print("please specify csv file path")

    # try:
    #     cnx.database = DB_NAME
    # except mariadb.Error as err:
    #     if err.errno == errorcode.ER_BAD_DB_ERROR:
    #         mariadb_create_database(cursor, DB_NAME)
    #         cnx.database = DB_NAME
    #     else:
    #         print(err)
    #         exit(1)
    # mariadb_create_face_table()
    # exit(0)

    facenet_model = 'facenet'
    facenet_fname = 'facenet_lfw_vector.csv'
    facenet_original_name = "feature_vector_facenet_lfw.csv"
    facenet_vlen = 128

    mxnet_model = 'mxnet'
    mxnet_fname = 'mxnet_lfw_vector.csv'
    mxnet_original_name = "feature_vector_mxnet_lfw.csv"
    mxnet_vlen = 256

    mxnet_hist_eqv_name = ""

    mxnet__aligned_original_name = "lfw_final_result/00mxnet_align.txt"

    # sys.argv = ["", "facenet_lfw_vector.csv", 128]
    # sys.argv = ["", "mxnet_lfw_vector.csv", 256]
    # evaluate("facenet", "facenet_lfw_vector.csv", 128)
    # evaluate("mxnet", "mxnet_lfw_vector.csv", 256)


    # eval3('lightcnn_with_align', "zihui/00mxnet_align.txt", "result")
    # eval3('lightcnn_with_histeq', "zihui/00mxnet_eqvhist.txt", "result")
    # eval3('lightcnn_original', "zihui/00mxnet_original.txt", "result")
    # eval3('lightcnn_with_align_histeq', "zihui/00mxnet_align_histeqv.txt", "result")

    # eval3('lfw_lightcnn_original', "lfw2/mxnet_original.txt", "result_lfw")
    # eval3('lfw_lightcnn_with_align', "lfw2/mxnet_align.txt", "result_lfw")
    # eval3('lfw_lightcnn_with_histeq', "lfw2/mxnet_eqvhist.txt", "result_lfw")
    eval3('lfw_lightcnn_with_align_histeq', "lfw2/mxnet_align_histeqv.txt", "result_lfw")
    pass
